#!/usr/bin/env python
from subprocess import getoutput, call
from os import chdir
from multiprocessing import Process, Lock
import fileinput
import itertools
import numpy as np
import numpy.linalg as LA
import time
import yaml
import re


def cal_eig(kpt_list):
    """
    return eigenvalue of the system at specific kpt points
    :param kpt_list: k points list, ndarray
    :return: eigenvalue list, ndarray
    """
    # read hr.dat
    with open('hr.dat', 'r') as file:
        # read num_wann and nrpts
        num_wann = int(file.readline().split()[0])
        nrpts = int(file.readline().split()[0])
        # read hamiltonian matrix elements
        rpt_list = []
        H_r = np.zeros((num_wann, num_wann, nrpts), dtype='complex')
        for i in range(nrpts):
            for j in range(num_wann):
                for k in range(num_wann):
                    buffer = file.readline().split()
                    # first index: band k, second index: band j, third index: rpt i
                    H_r[k, j, i] = float(buffer[5]) + 1j * float(buffer[6])
            rpt_list = rpt_list + [buffer[0:3]]
        rpt_list = np.array(rpt_list, dtype='float')
    # read r_ndegen.dat
    with open('rndegen.dat', 'r') as file:
        buffer = file.read().split()
        r_ndegen = np.array([int(ndegen) for ndegen in buffer], dtype='float')
    # calculate H_w
    nkpts = kpt_list.shape[0]
    phase = 1j * np.dot(rpt_list, kpt_list.T)
    phase = np.exp(phase) / r_ndegen[:, None]
    H_w = np.tensordot(H_r, phase, axes=1)
    # calculate eigenvalues
    eigenvalues = np.zeros((num_wann, nkpts))
    for i in range(nkpts):
        (w, v) = LA.eig(H_w[:, :, i])
        idx = w.argsort()
        w = np.real(w[idx])
        eigenvalues[:, i] = np.real(w)
    return eigenvalues


def cal_min_err(fp_eig, wannier_eig):
    all_comb = itertools.combinations(wannier_eig, len(fp_eig))
    wannier_eig.sort()
    err = None
    for comb in all_comb:
        comb = list(comb)
        comb.sort()
        err_temp = LA.norm(np.array(comb) - np.array(fp_eig))
        if err is None:
            err = err_temp
        else:
            if err_temp < err:
                err = err_temp
    return err


def worker(dis_win_min, dis_win_max, dis_froz_min, dis_froz_max, config, cnt, l):
    call(['mkdir', str(cnt)])
    files = config['files']
    # copy files
    for file in files:
        call(['cp', file, str(cnt)])
    chdir(str(cnt))
    # write wannier input file and submit jobs
    for line in fileinput.input(config['seedname'] + '.win', inplace=True):
        print(line.replace('dis_win_min', 'dis_win_min = ' + str(dis_win_min)), end="")
    for line in fileinput.input(config['seedname'] + '.win', inplace=True):
        print(line.replace('dis_win_max', 'dis_win_max = ' + str(dis_win_max)), end="")
    for line in fileinput.input(config['seedname'] + '.win', inplace=True):
        print(line.replace('dis_froz_min', 'dis_froz_min = ' + str(dis_froz_min)), end="")
    for line in fileinput.input(config['seedname'] + '.win', inplace=True):
        print(line.replace('dis_froz_max', 'dis_froz_max = ' + str(dis_froz_max)), end="")
    job_id = getoutput('qsub ' + config['run']).split('.')[0]
    # check output
    while True:
        mjobs_out = getoutput('mjobs')
        if ' ' + str(job_id) + ' ' not in mjobs_out:
            if 'All done' not in getoutput('tail -n 1 ' + config['seedname'] + '.wout'):
                l.acquire()
                try:
                    print('{0:4d} {1:12f} {2:12f} {3:12f} {4:12f}'
                          .format(cnt, dis_win_min, dis_win_max, dis_froz_min, dis_froz_max))
                finally:
                    l.release()
                    break
            else:
                dis_str = getoutput("grep ' <-- DIS' " + config['seedname'] + '.wout') + '\n'
                conv_str = getoutput("grep ' <-- CONV' " + config['seedname'] + '.wout') + '\n'
                dis_str_list = dis_str.split('<-- DIS\n')[:-1]
                conv_str_list = conv_str.split('<-- CONV\n')[:-1]
                dis_del_list = np.array([float(dis_str.split()[3]) for dis_str in dis_str_list])
                dis_del_min = np.min(np.concatenate((np.abs(dis_del_list[dis_del_list > 0]), np.array([1]))))
                conv_del_list = np.array([float(conv_str.split()[1]) for conv_str in conv_str_list])
                conv_del_min = np.min(np.concatenate((np.abs(conv_del_list[conv_del_list < 0]), np.array([1]))))
                spread_min = np.min(np.array([float(conv_str.split()[3]) for conv_str in conv_str_list]))
                # calculate eigenvalues
                call(['wantop.x', 'wannier90'])
                kpt_list = np.array(config['kpt_list'])
                fp_eig = np.array(config['eig'])
                wannier_eig = cal_eig(kpt_list)
                err = 0
                for i in range(len(fp_eig)):
                    err += cal_min_err(fp_eig[i], wannier_eig[:, i])
                # print result
                l.acquire()
                try:
                    print(
                        '{0:4d} {1:12f} {2:12f} {3:12f} {4:12f} {5:15E} {6:15E} {7:20f} {5:10f}'
                            .format(cnt, dis_win_min, dis_win_max, dis_froz_min,
                                    dis_froz_max, dis_del_min, conv_del_min, spread_min, err)
                        )
                finally:
                    l.release()
                    break
        else:
            time.sleep(config['check_finish_interval'])


if __name__ == '__main__':

    def get_job_num(identifier):
        mjobs_out = getoutput('mjobs')
        return len(re.findall(identifier, mjobs_out))
    # open config file
    with open('metawann.in') as file:
        config = file.read()
    config = yaml.load(config)

    # construct dis_win
    dis_win_min_list = np.linspace(config['dis_win_min_min'], config['dis_win_min_max'], config['dis_win_min_ndiv'])
    dis_win_max_list = np.linspace(config['dis_win_max_min'], config['dis_win_max_max'], config['dis_win_max_ndiv'])
    dis_froz_max_list = np.linspace(config['dis_froz_max_min'], config['dis_froz_max_max'], config['dis_froz_max_ndiv'])
    dis_froz_min_list = np.linspace(config['dis_froz_min_min'], config['dis_froz_min_max'], config['dis_froz_min_ndiv'])

    # iterating dis_win to get optimization
    identifier = config['job_name']
    cnt = 0
    lock = Lock()
    jobs = []
    for dis_win_min in dis_win_min_list:
        for dis_win_max in dis_win_max_list:
            for dis_froz_min in dis_froz_min_list:
                for dis_froz_max in dis_froz_max_list:
                    if get_job_num(identifier) < config['job_num']:
                        job = Process(target=worker, args=(dis_win_min, dis_win_max, dis_froz_min, dis_froz_max, config, cnt, lock))
                        jobs.append(job)
                        job.start()
                        cnt += 1
                        time.sleep(config['cp_time'])
                    else:
                        time.sleep(config['check_job_num_interval'])
    for job in jobs:
        job.join()